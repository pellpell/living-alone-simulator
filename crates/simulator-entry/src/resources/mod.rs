// crates/simulator-entry/src/resources/mod.rs

pub use {entry::*, entry_assets::*};

mod entry;
mod entry_assets;
