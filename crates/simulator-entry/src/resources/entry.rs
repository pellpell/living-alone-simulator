// crates/simulator-entry/src/resources/entry.rs

use bevy::prelude::*;

#[derive(Debug)]
pub struct Entry(pub Entity);
