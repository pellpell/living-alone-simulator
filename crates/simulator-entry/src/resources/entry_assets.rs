// crates/simulator-entry/src/resources/entry_assets.rs

use bevy::prelude::*;

#[derive(Debug, Clone)]
pub struct EntryAssets {
    pub label: String,
    pub title_font: Handle<Font>,
    pub button_font: Handle<Font>,
}
