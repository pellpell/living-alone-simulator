// crates/simulator-entry/src/lib.rs

pub mod buttons;
pub mod resources;

use buttons::{ButtonAction, ButtonColors};
use resources::Entry;
use resources::EntryAssets;

use bevy::log;
use bevy::prelude::*;

use bevy::ecs::schedule::StateData;

pub struct EntryPlugin<T> {
    pub running_state: T,
}

impl<T: StateData> Plugin for EntryPlugin<T> {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_system_set(
            SystemSet::on_enter(self.running_state.clone()).with_system(Self::create_entry),
        );

        app.add_system_set(
            SystemSet::on_exit(self.running_state.clone()).with_system(Self::cleanup_entry),
        );

        log::info!("Loaded Entry Plugin");
    }
}

impl<T> EntryPlugin<T> {
    fn create_entry(
        mut commands: Commands,
        entry_assets: Res<EntryAssets>,
        button_colors: Res<ButtonColors>,
    ) {
        log::info!("Create Entry");

        let entry = commands
            .spawn_bundle(NodeBundle {
                style: Style {
                    size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
                    justify_content: JustifyContent::SpaceBetween,
                    ..default()
                },
                color: Color::BISQUE.into(),
                ..default()
            })
            .insert(Name::new("Entry"))
            .with_children(|parent| {
                // タイトル
                parent
                    .spawn_bundle(NodeBundle {
                        style: Style {
                            size: Size::new(Val::Percent(60.), Val::Px(100.)),
                            align_items: AlignItems::Center,
                            justify_content: JustifyContent::Center,
                            position_type: PositionType::Absolute,
                            position: UiRect {
                                top: Val::Percent(20.),
                                left: Val::Percent(20.),
                                ..Default::default()
                            },
                            ..Default::default()
                        },
                        color: Color::GREEN.into(),
                        ..Default::default()
                    })
                    .insert(Name::new("TitleUI"))
                    .with_children(|parent| {
                        let font = entry_assets.title_font.clone();
                        Self::setup_title(parent, "ひとり暮らしシミュレーター", font);
                    });

                // ボタンメニュー
                parent
                    .spawn_bundle(NodeBundle {
                        style: Style {
                            size: Size::new(Val::Percent(30.), Val::Px(300.)),
                            flex_direction: FlexDirection::ColumnReverse,
                            align_items: AlignItems::Center,
                            justify_content: JustifyContent::Center,
                            position_type: PositionType::Absolute,
                            position: UiRect {
                                top: Val::Percent(45.),
                                left: Val::Percent(35.),
                                ..Default::default()
                            },
                            ..Default::default()
                        },
                        color: Color::NONE.into(),
                        ..Default::default()
                    })
                    .insert(Name::new("ButtonMenuUI"))
                    .with_children(|parent| {
                        let font = entry_assets.button_font.clone();
                        Self::setup_single_menu(
                            parent,
                            "はじめから",
                            button_colors.normal.into(),
                            font.clone(),
                            ButtonAction::Opening,
                        );
                        Self::setup_single_menu(
                            parent,
                            "つづきから",
                            button_colors.normal.into(),
                            font.clone(),
                            ButtonAction::Continue,
                        );
                        Self::setup_single_menu(
                            parent,
                            "設定",
                            button_colors.normal.into(),
                            font,
                            ButtonAction::Setting,
                        );
                    });
            })
            .id();

        commands.insert_resource(Entry(entry));
    }

    fn setup_title(parent: &mut ChildBuilder, text: &str, font: Handle<Font>) {
        parent
            .spawn_bundle(ButtonBundle {
                style: Style {
                    size: Size::new(Val::Percent(100.), Val::Percent(100.)),
                    justify_content: JustifyContent::Center,
                    align_items: AlignItems::Center,
                    ..Default::default()
                },
                color: bevy::prelude::UiColor(Color::ORANGE_RED),
                ..Default::default()
            })
            .insert(Name::new(text.to_string()))
            .with_children(|builder| {
                builder.spawn_bundle(TextBundle {
                    text: Text {
                        sections: vec![TextSection {
                            value: text.to_string(),
                            style: TextStyle {
                                font,
                                font_size: 60.,
                                color: Color::WHITE,
                            },
                        }],
                        alignment: TextAlignment {
                            vertical: VerticalAlign::Center,
                            horizontal: HorizontalAlign::Center,
                        },
                    },
                    ..Default::default()
                });
            });
    }

    fn setup_single_menu(
        parent: &mut ChildBuilder,
        text: &str,
        color: UiColor,
        font: Handle<Font>,
        action: ButtonAction,
    ) {
        parent
            .spawn_bundle(NodeBundle {
                style: Style {
                    size: Size::new(Val::Percent(100.), Val::Percent(100.)),
                    margin: UiRect::all(Val::Px(10.)),
                    border: UiRect::all(Val::Px(10.)),
                    ..default()
                },
                color: Color::GREEN.into(),
                ..default()
            })
            .with_children(|parent| {
                parent
                    .spawn_bundle(ButtonBundle {
                        style: Style {
                            size: Size::new(Val::Percent(100.), Val::Percent(100.)),
                            justify_content: JustifyContent::Center,
                            align_items: AlignItems::Center,
                            ..Default::default()
                        },
                        color,
                        ..Default::default()
                    })
                    .insert(action)
                    .insert(Name::new(text.to_string()))
                    .with_children(|builder| {
                        builder.spawn_bundle(TextBundle {
                            text: Text {
                                sections: vec![TextSection {
                                    value: text.to_string(),
                                    style: TextStyle {
                                        font,
                                        font_size: 40.,
                                        color: Color::WHITE,
                                    },
                                }],
                                alignment: TextAlignment {
                                    vertical: VerticalAlign::Center,
                                    horizontal: HorizontalAlign::Center,
                                },
                            },
                            ..Default::default()
                        });
                    });
            });
    }

    fn cleanup_entry(entry: Res<Entry>, mut commands: Commands) {
        commands.entity(entry.0).despawn_recursive();
        commands.remove_resource::<Entry>();
    }
}
