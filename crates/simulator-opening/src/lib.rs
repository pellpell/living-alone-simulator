// crates/simulator-opening/src/lib.rs

use bevy::log;
use bevy::prelude::*;

use bevy::ecs::schedule::StateData;

pub struct OpeningPlugin<T> {
    pub running_state: T,
}

impl<T: StateData> Plugin for OpeningPlugin<T> {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_system_set(
            SystemSet::on_enter(self.running_state.clone()).with_system(Self::create_opening),
        );

        app.add_system_set(
            SystemSet::on_exit(self.running_state.clone()).with_system(Self::cleanup_opening),
        );

        log::info!("Loaded Opening Plugin");
    }
}

impl<T> OpeningPlugin<T> {
    fn create_opening() {
        log::info!("Create Opening");
    }

    fn cleanup_opening() {
        log::info!("Cleanup Opening");
    }
}
