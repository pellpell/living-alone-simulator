// src/simulator_app_state.rs

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum SimulatorAppState {
    Entry,
    Opening,
}
