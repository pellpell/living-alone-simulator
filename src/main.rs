// src/main.rs

mod simulator_app_state;
use simulator_app_state::SimulatorAppState;

use simulator_entry::buttons::{ButtonAction, ButtonColors};
use simulator_entry::resources::EntryAssets;
use simulator_entry::EntryPlugin;
use simulator_opening::OpeningPlugin;

use bevy::log;
use bevy::log::{Level, LogSettings};
use bevy::prelude::*;

#[cfg(feature = "debug")]
use bevy_inspector_egui::WorldInspectorPlugin;

fn main() {
    const WINDOW_TITLE: &str = "Living Alone Simulator";
    const WINDOW_WIDTH: f32 = 1200.;
    const WINDOW_HEIGHT: f32 = 800.;

    let mut app = App::new();

    let level = if cfg!(feature = "debug") {
        Level::DEBUG
    } else {
        Level::INFO
    };

    // Window setup
    app.insert_resource(WindowDescriptor {
        title: WINDOW_TITLE.to_string(),
        width: WINDOW_WIDTH,
        height: WINDOW_HEIGHT,
        ..Default::default()
    })
    .insert_resource(LogSettings {
        level,
        ..Default::default()
    })
    .add_plugins(DefaultPlugins);

    #[cfg(feature = "debug")]
    app.add_plugin(WorldInspectorPlugin::new());

    app.add_state(SimulatorAppState::Entry)
        .add_plugin(OpeningPlugin {
            running_state: SimulatorAppState::Opening,
        })
        .add_plugin(EntryPlugin {
            running_state: SimulatorAppState::Entry,
        })
        .add_startup_system(camera_setup)
        .add_startup_system(setup_entry)
        .add_system(input_handler);

    app.run();
}

fn camera_setup(mut commands: Commands) {
    commands.spawn_bundle(Camera2dBundle::default());
}

#[allow(clippy::type_complexity)]
fn input_handler(
    button_colors: Res<ButtonColors>,
    mut interaction_query: Query<
        (&Interaction, &ButtonAction, &mut UiColor),
        (Changed<Interaction>, With<Button>),
    >,
    mut state: ResMut<State<SimulatorAppState>>,
) {
    for (interaction, action, mut color) in interaction_query.iter_mut() {
        match *interaction {
            Interaction::Clicked => {
                *color = button_colors.pressed.into();
                match action {
                    ButtonAction::Opening => {
                        log::debug!("opening detected");
                        if state.current() == &SimulatorAppState::Entry {
                            log::info!("set opening");
                            state.set(SimulatorAppState::Opening).unwrap();
                        }
                    },
                    ButtonAction::Continue => {
                        log::debug!("continue detected");
                        if state.current() == &SimulatorAppState::Entry {
                            log::info!("continue clicked!");
                        }
                    },
                    ButtonAction::Setting => {
                        log::debug!("setting detected");
                        if state.current() == &SimulatorAppState::Entry {
                            log::info!("setting clicked!");
                        }
                    },
                }
            }
            Interaction::Hovered => {
                *color = button_colors.hovered.into();
            }
            Interaction::None => {
                *color = button_colors.normal.into();
            }
        }
    }
}

fn setup_entry(mut commands: Commands, asset_server: Res<AssetServer>) {
    const JP_FONT: &str = "fonts/x12y16pxMaruMonica.ttf";
    commands.insert_resource(EntryAssets {
        label: "EntryAssets".to_string(),
        button_font: asset_server.load(JP_FONT),
        title_font: asset_server.load(JP_FONT),
    });

    commands.insert_resource(ButtonColors {
        normal: Color::LIME_GREEN,
        hovered: Color::rgb(0.2, 0.6, 0.2),
        pressed: Color::DARK_GREEN,
    });
}
